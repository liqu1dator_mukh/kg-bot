# "Когда КГ?" discord bot

Так когда же [КГ](https://кубград.рф)? 🤔

# Что?

Локальный мем

## Сборка

Установите [Rust](https://www.rust-lang.org/learn/get-started), git

```bash
git clone https://gitlab.com/liqu1dator_mukh/kg-bot.git
cd kg-bot 
RUSTFLAGS="-Ctarget-cpu=x86-64-v2 --cfg tokio_unstable" cargo build --release
```

## Установка

TODO: cargo-dep

## Пользование

```bash
kg-bot --c config.toml
```

Пример конфига лежит в config

Если аргумент не указан, то файл ищется в текущей директории.

## Contributing

Я даже представляю что тут можно законтрибьютить
