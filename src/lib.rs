#![allow(clippy::str_to_string)]
pub mod when_kg;

use thiserror::Error;
use miette::Diagnostic;
// User data, which is stored and accessible in all command invocations
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct _Data;

#[derive(Debug, Clone, Copy, Error, Diagnostic)]
pub enum Error {
    #[error("Failed to add time to date")]
    FailedApplyTimeToDate

}

pub type Context<'a> = poise::Context<'a, _Data, miette::Error>;

use chrono::{DateTime, FixedOffset};
use miette::IntoDiagnostic;
use tokio::sync::OnceCell;

pub static DATE: OnceCell<DateTime<FixedOffset>> = OnceCell::const_new();

#[poise::command(prefix_command, owners_only)]
pub async fn register(ctx: Context<'_>) -> Result<(), miette::Error> {
    poise::builtins::register_application_commands_buttons(ctx).await.into_diagnostic()?;
    Ok(())
}
