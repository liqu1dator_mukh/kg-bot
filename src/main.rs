mod args;
mod config;

use crate::args::Args;
use crate::config::parse_config;
use clap::Parser;
use miette::IntoDiagnostic;
use kg_bot::when_kg::{kg_when, kg_when_sender};
use kg_bot::{_Data, register, DATE};
use poise::serenity_prelude::Http;
use poise::{serenity_prelude as serenity, PrefixFrameworkOptions};
use rewrap::Rewrap;
use serenity::GatewayIntents;
use tokio::join;
use tracing::{error, info};
use tracing_subscriber::FmtSubscriber;

#[derive(Debug, strum::EnumString, Default)]
pub enum Logger {
    #[default]
    Tracing,
    Console,
}

#[tokio::main]
async fn main() -> Result<(), miette::Error> {
    info!("Parsing args...");
    let args = Args::parse();

    match args.logger {
        Logger::Tracing => {
            let sub = FmtSubscriber::builder()
                .compact()
                .with_max_level(args.logger_level)
                .finish();
            tracing::subscriber::set_global_default(sub).into_diagnostic()?;
        }
        Logger::Console => {
            console_subscriber::init();
        }
    };

    let (token, date, scheduled_time, channel_id) = parse_config(args.config_path)?;

    info!(
        "Config parsed: first letter of token: \"{}\", date: \"{}\", channel_id: {}",
        &token.chars().next().unwrap(),
        date,
        channel_id
    );

    info!("Setting date globally");
    DATE.set(date).into_diagnostic()?;
    info!("Setting date done: {}", date);

    let commands = vec![register(), kg_when()];
    info!("Commands: {:?}", commands);

    info!("Creating framework...");
    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            prefix_options: PrefixFrameworkOptions {
                prefix: Some(String::from("--")),
                ..Default::default()
            },
            commands,
            ..Default::default()
        })
        .token(&token)
        .intents(GatewayIntents::MESSAGE_CONTENT | GatewayIntents::GUILD_MESSAGES)
        .user_data_setup(move |_ctx, _ready, _framework| Box::pin(async move { Ok(_Data) }));

    info!("Creating http client for когдакг message");
    let http = Http::new(&token);

    info!("Spawning tasks...");
    let (framework, kogda_kg) = join!(
        tokio::spawn(framework.run()),
        tokio::spawn(kg_when_sender(channel_id, http, scheduled_time))
    );

    framework.into_diagnostic()?// TODO: handle panic
        .unwrap_or_else(|why| error!("Framework failed: {why}"));
    kogda_kg.into_diagnostic().rewrap().unwrap_or_else(|why| {
        error!("Failed to send kogda kg: {why}");
    });

    info!("Shutdown...");

    Ok(())
}
