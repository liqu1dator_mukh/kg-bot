use crate::Logger;
use std::path::PathBuf;

#[derive(clap::Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    #[clap(
        short,
        long,
        value_parser,
        value_name = "FILE",
        default_value = "config.toml"
    )]
    pub config_path: PathBuf,
    #[clap(short, long, value_name = "LOGGER", default_value = "Tracing")]
    pub logger: Logger,
    #[clap(long, value_name = "LOGGER_LEVEL", default_value = "Info")]
    pub logger_level: tracing::Level,
}
