use chrono::{DateTime, FixedOffset, NaiveTime};
use miette::{Context, IntoDiagnostic};
use poise::serenity_prelude::ChannelId;
use serde::Deserialize;
use std::fs;
use std::path::PathBuf;
use tracing::info;

#[derive(Debug, Deserialize)]
struct Config<'date> {
    date: &'date str,
    token_path: PathBuf,
    channel_id: u64, // sending msg to thread_id is same as channel_id
    when_to_send_utc_time: &'date str,
}

#[tracing::instrument]
pub fn parse_config(
    config_path: PathBuf,
) -> Result<(String, DateTime<FixedOffset>, NaiveTime, ChannelId), miette::Error> {
    let raw = fs::read(&config_path).into_diagnostic().wrap_err("Failed to read config")?;
    let config: Config = toml::from_slice(&raw).into_diagnostic().wrap_err("Failed to parse config into TOML object")?;

    info!("config file ({:?}) parsed: {:?}", &config_path, &config);

    let date = DateTime::parse_from_rfc3339(config.date)
        .into_diagnostic()
        .wrap_err(format!("Failed to parse field date in file: {config_path:?}"))?;
    
    let scheduled_time = NaiveTime::parse_from_str(config.when_to_send_utc_time, "%H:%M:%S")
        .into_diagnostic()
        .wrap_err(format!("Failed to parse field time in file {config_path:?}"))?;

    let token = fs::read_to_string(config.token_path).into_diagnostic().wrap_err("Failed to read token")?;

    Ok((token, date, scheduled_time, ChannelId(config.channel_id)))
}
