#![allow(clippy::str_to_string)]

use crate::Error;
use crate::{Context, DATE};
use chrono::{Duration, NaiveTime, Utc};
use miette::IntoDiagnostic;
use poise::serenity_prelude::{ChannelId, Http};
use std::fmt::Display;
use tokio::time;
use tracing::info;

#[tracing::instrument]
#[must_use]
pub fn msg() -> impl Display {
    let start_date = *DATE.get().expect("Date is not initialized");
    let duration = Utc::now().signed_duration_since(start_date);
    let days_passed = duration.num_days();

    format!("День {days_passed}/∞ \nОсталось ∞ дней")
}

#[tracing::instrument]
pub async fn kg_when_sender(
    channel: ChannelId,
    http: Http,
    scheduled_time: NaiveTime,
) -> Result<(), miette::Error> {
    const SECS_IN_HOUR: u32 = 60 * 60;
    const SECS_IN_DAY: u32 = 24 * SECS_IN_HOUR;

    let scheduled_day = if Utc::now().time() < scheduled_time {
        let output = Utc::today();
        info!("Scheduled time still can be accessed today, setting scheduled_day to {output}");
        output
    } else {
        let output = Utc::today() + Duration::days(1);
        info!("Scheduled time cannot be accessed today, setting scheduled_day to {output}");
        output
    };

    let scheduled_day_with_time = scheduled_day
        .and_time(scheduled_time)
        .ok_or(Error::FailedApplyTimeToDate)?;

    let duration_until_scheduled_time = scheduled_day_with_time.signed_duration_since(Utc::now());
    info!(
        "Gonna sleep until UTC {} ({})",
        scheduled_day_with_time,
        duration_until_scheduled_time.to_string()
    );

    time::sleep(duration_until_scheduled_time.to_std().into_diagnostic()?).await;

    info!("alarm moment: i'm woke");

    let mut day = tokio::time::interval(time::Duration::from_secs(u64::from(SECS_IN_DAY)));

    loop {
        day.tick().await;
        channel.say(&http, msg()).await.into_diagnostic()?;
        info!("Когда кг message sent");
        info!("Gonna sleep {}", day.period().as_secs());
    }
}

#[tracing::instrument]
#[poise::command(
    slash_command,
    rename = "когдакг",
    required_bot_permissions = "SEND_MESSAGES"
)]

pub async fn kg_when(ctx: Context<'_>) -> Result<(), miette::Error> {
    ctx.say(msg().to_string()).await.into_diagnostic()?; // impl Display does not implement Into<String>? wtf
    Ok(())
}
